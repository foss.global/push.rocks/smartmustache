import { tap, expect } from '@pushrocks/tapbundle';

import * as tlt from '../ts/index.js';

let testMustache: tlt.SmartMustache;
tap.test('should create a valid instance of tlt', async () => {
  testMustache = new tlt.SmartMustache(
    'some awesome {{customString}} that is {{license}} licensed'
  );
  expect(testMustache).toBeInstanceOf(tlt.SmartMustache);
});
tap.test('should output a valid string with some data', async () => {
  let appliedString = testMustache.applyData({
    customString: 'horse',
    license: 'MIT',
  });
  expect(appliedString).toEqual('some awesome horse that is MIT licensed');
});

tap.start();
